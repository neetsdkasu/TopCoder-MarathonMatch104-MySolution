import java.io.*;
import java.util.*;

public class TypesetterArt {

    static final int LINE_CHAR_CODE  =   10;
    static final int SPACE_CHAR_CODE =   32;
    static final int MIN_CHAR_CODE   =   32;
    static final int MAX_CHAR_CODE   =  126;
    static final int MIN_FONT_SIZE   =    8;
    static final int MAX_FONT_SIZE   =   72;
    static final int MAX_COLOR_VALUE = 0xFF;
    static boolean RUNNING_ON_SERVER = true;

    RandomEx rand = new RandomEx(19831983L);
    int nRenders, nFonts, precision;
    int width, height;
    short images[][][] = new short[4][][];
    short layers[][][] = new short[4][][];

    public int init(int nRenders, int nFonts, int precision, int width, int height, int[] image) {
        this.nRenders  = nRenders;
        this.nFonts    = nFonts;
        this.precision = precision;
        this.width     = width;
        this.height    = height;

        System.err.printf("nRenders: %d, nFonts: %d, precision: %d, width: %d, height:%d %n",
            nRenders, nFonts, precision, width, height);

        int index = 0;
        for (int k = 0; k < 4; k++) {
            layers[k] = new short[height][width];
            short[][] tmp = new short[height][width];
            for (int y = 0; y < height; y++) {
                for (int x = 0; x < width; x++) {
                    tmp[y][x] = (short)Math.min(MAX_COLOR_VALUE, image[index]);
                    index++;
                }
            }
            images[k] = tmp;
        }

        double score = calcScore(layers);
        System.err.printf("score is %f%n", score);

        return 0;
    }

    int ngFontId   = 0;
    int ngFontSize = 0;
    int ngLetterId = 0;
    Font[][] fonts = null;

    void initNextGryph() {
        ngFontId   = 0;
        ngFontSize = MIN_FONT_SIZE;
        ngLetterId = MIN_CHAR_CODE;
        fonts = new Font[nFonts][MAX_FONT_SIZE + 1];
        for (int id = 0; id < nFonts; id++) {
            for (int size = MIN_FONT_SIZE; size <= MAX_FONT_SIZE; size++) {
                fonts[id][size] = new Font();
            }
        }
    }

    public int[] nextGlyph(int width, int height, int[] bitmask, int measuredMillis) {

        if (ngFontSize == 0) {
            initNextGryph();
        } else {
            Font f = fonts[ngFontId][ngFontSize];
            f.height = (short)height;
            f.gryphs[ngLetterId] = new Gryph(width, height, bitmask);
            f.updateWidth(width);

            ngLetterId++;
            if (ngLetterId > MAX_CHAR_CODE) {
                ngLetterId = MIN_CHAR_CODE;
                ngFontSize++;
                if (ngFontSize > MAX_FONT_SIZE) {
                    ngFontSize = MIN_FONT_SIZE;
                    ngFontId++;
                }
            }
        }

        if (ngFontId < nFonts) {
            return new int[]{ ngLetterId, ngFontId, ngFontSize };
        } else {
            return new int[0];
        }
    }

    public int[] render(int iteration, int measuredMillis) {

        int[] ret;

        // ret = solveRandom(); // random
        // ret = solveHighestFillRate(iteration, false, 1, 2); // same 3rd-submit (5th-submit)
        // ret = solveHighestFillRate(iteration, true, 1, 2);  // same 2nd-submit
        // ret = solveHighestFillRate(iteration, true, 3, 4);  // 6th-submit
        // ret = solveLike4thSubmit(iteration);  // like 4th-submit, not completely same
        ret = solveGreedy(iteration, measuredMillis);

        // double score = calcScore(layers);
        // System.err.printf("score is %f%n", score);

        return ret;
    }

    double calcScore(short[][][] target) {
        double score = 0;
        int dv = 0;
        for (int k = 0; k < 4; k++) {
            TpDI p = calcColorScore(images[k], target[k]);
            score += p.item1;
            dv += p.item2;
        }
        return dv > 0 ? score / (double)dv * 1000000.0 : 0;
    }

    TpDI calcColorScore(short[][] tmp1, short[][] tmp2) {
        double score = 0;
        int dv = 0;
        for (int y = 0; y < height; y += precision) {
            for (int x = 0; x < width; x += precision) {
                int sum1 = 0;
                int sum2 = 0;
                int ey = Math.min(y + precision, height);
                int ex = Math.min(x + precision, width);
                int n = (ex - x) * (ey - y);
                for (int ty = y; ty < ey; ty++) {
                    for (int tx = x; tx < ex; tx++) {
                        sum1 += (int)tmp1[ty][tx];
                        sum2 += Math.min(MAX_COLOR_VALUE, (int)tmp2[ty][tx]);
                    }
                }
                if (n > 0) {
                    double e1 = (double)sum1 / (double)(n * MAX_COLOR_VALUE);
                    double e2 = (double)sum2 / (double)(n * MAX_COLOR_VALUE);
                    double s = 1.0 - Math.abs(e1 - e2);
                    score += s * s;
                    dv++;
                }
            }
        }
        return new TpDI(score, dv);
    }

    double calcPartialScore(short[][] tmp1, short[][] tmp2, int x0, int y0, int w, int h) {
        double score = 0;
        int dv = 0;
        x0 = (x0 / precision) * precision;
        y0 = (y0 / precision) * precision;
        int x1 = Math.min(width, ((x0 + w + precision - 1) / precision) * precision);
        int y1 = Math.min(height, ((y0 + h + precision - 1) / precision) * precision);
        for (int y = y0; y < y1; y += precision) {
            for (int x = x0; x < x1; x += precision) {
                int ex = Math.min(x + precision, width);
                int ey = Math.min(y + precision, height);
                int sum1 = 0, sum2 = 0;
                int n = (ex - x) * (ey - y);
                for (int ty = y; ty < ey; ty++) {
                    for (int tx = x; tx < ex; tx++) {
                        sum1 += (int)tmp1[ty][tx];
                        sum2 += (int)tmp2[ty][tx];
                    }
                }
                if (n > 0) {
                    double e1 = (double)sum1 / (double)(n * MAX_COLOR_VALUE);
                    double e2 = (double)sum2 / (double)(n * MAX_COLOR_VALUE);
                    double s = 1.0 - Math.abs(e1 - e2);
                    score += s * s;
                    dv++;
                }
            }
        }
        return dv > 0 ? score / (double)dv : 0;
    }

    int[] solveRandom() {
        IntList ret = new IntList(60000, 10000);

        int   fontId     = rand.nextInt(nFonts);
        int   fontSize   = rand.nextInt(MIN_FONT_SIZE, MAX_FONT_SIZE + 1);
        int   colorType  = rand.nextInt(4);
        short colorValue = (short)rand.nextInt(MAX_COLOR_VALUE + 1);

        ret.add(fontId);
        ret.add(fontSize);
        ret.add(colorType);
        ret.add((int)colorValue);
        Font f = fonts[fontId][fontSize];
        short[][] tmp = layers[colorType];
        int offY = 0;
        for (int i = 0; i < 400; i++) {
            int h = Math.min(f.height, height - offY);
            int offX = 0;
            for (int j = 0; j < 400; j++) {
                int charCode = rand.nextInt(MIN_CHAR_CODE, MAX_CHAR_CODE + 1);
                Gryph g = f.gryphs[charCode];
                ret.add(charCode);
                int w = Math.min(g.width, width - offX);
                for (int y = 0; y < h; y++) {
                    for (int x = 0; x < w; x++) {
                        if (g.exists(x, y)) {
                            tmp[y + offY][x + offX] += colorValue;
                        }
                    }
                }
                offX += g.width;
                if (offX >= width) {
                    break;
                }
            }
            ret.add(LINE_CHAR_CODE);
            offY += f.height;
            if (offY >= height) {
                break;
            }
        }
        return ret.toArray();
    }

    int[]  hfrMeds     = new int[4];
    int    hfrFontId   =  0;
    int    hfrFontSize =  8;
    int    hfrLetter   = 65;
    double hfrFill     =  0;

    void initHFR(int m, int d) {
        for (int k = 0; k < 4; k++) {
            hfrMeds[k] = (int)findMedian(images[k], m, d);
        }

        TpDI selChar = findHighestFillRateChar(hfrFontSize);
        hfrFontId = selChar.item2 % nFonts;
        hfrLetter = selChar.item2 / nFonts;
        hfrFill   = selChar.item1;
    }

    short findMedian(short[][] tmp, int m, int d) {
        short[] xs = new short[height * width];
        int index = 0;
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                xs[index] = tmp[y][x];
                index++;
            }
        }
        Arrays.sort(xs);
        return xs[xs.length * m / d];
    }

    TpDI findHighestFillRateChar(int size) {
        return findHighestFillRateChar(size, true);
    }

    TpDI findHighestFillRateChar(int size, boolean anyPitch) {
        int    selFontId =  0;
        int    selLetter = 65;
        double selFill   =  0;
        boolean selProp  = false;
        for (int id = 0; id < nFonts; id++) {
            Font f = fonts[id][size];
            if (f.gryphs[SPACE_CHAR_CODE].width == 0) {
                continue;
            }
            boolean prop = f.isProportional();
            for (int i = MIN_CHAR_CODE; i <= MAX_CHAR_CODE; i++) {
                Gryph g = f.gryphs[i];
                int count = g.count();
                double fill = (double)count / (double)(g.width * f.height);
                if (((anyPitch || selProp == prop) && fill > selFill) || (!anyPitch & !selProp & prop)) {
                    selFontId = id;
                    selLetter = i;
                    selFill = fill;
                    selProp = prop;
                }
            }
        }
        return new TpDI(selFill, selLetter * nFonts + selFontId);
    }

    int[] solveHighestFillRate(int iteration, boolean useFixedRate, int m, int d) {
        IntList ret = new IntList(60000, 10000);

        if (iteration == 0) {
            initHFR(m, d);
            if (useFixedRate) {
                hfrFill = 0.5;
            }
        }

        int p = iteration % 4;

        Font  f  = fonts[hfrFontId][hfrFontSize];
        Gryph g  = f.gryphs[hfrLetter];
        Gryph sp = f.gryphs[SPACE_CHAR_CODE];
        short[][] tmp1 = images[p];
        short med = (short)hfrMeds[p];

        ret.add(hfrFontId);
        ret.add(hfrFontSize);
        ret.add(p);
        ret.add((int)med);

        for (int y = 0; y < height; y += f.height) {
            int x = 0;
            int ey = Math.min(y + f.height, height);
            while (x < width) {
                int sm = 0;
                int ex = Math.min(x + g.width, width);
                int n = (ex - x) * (ey - y);
                for (int ty = y; ty < ey; ty++) {
                    for (int tx = x; tx < ex; tx++) {
                        sm += (int)tmp1[ty][tx];
                    }
                }
                if ((double)sm < hfrFill * (double)(n * (int)med)) {
                    ret.add(SPACE_CHAR_CODE);
                    x += sp.width;
                } else {
                    for (int ty = y; ty < ey; ty++) {
                        for (int tx = x; tx < ex; tx++) {
                            if (tmp1[ty][tx] < med) {
                                tmp1[ty][tx] = 0;
                            } else {
                                tmp1[ty][tx] -= med;
                            }
                        }
                    }
                    ret.add(hfrLetter);
                    x += g.width;
                }
            }
            ret.add(LINE_CHAR_CODE);
        }

        hfrMeds[p] = (int)findMedian(tmp1, m, d);

        return ret.toArray();
    }

    Solution[] l4sSolutions = null;

    int[] solveLike4thSubmit(int iteration) {
        if (iteration == 0) {
            initL4S();
        }
        return l4sSolutions[iteration].get();
    }

    int[]    l4sMeds    = new int[4];
    int[]    l4sSizes   = { 8, 9, 14, 12, 10, 20 };
    int[]    l4sFontIds = new int[l4sSizes.length];
    int[]    l4sLettrs  = new int[l4sSizes.length];
    double[] l4sFills   = new double[l4sSizes.length];

    void initL4S() {
        for (int k = 0; k < 4; k++) {
            l4sMeds[k] = (int)findMedian(images[k], 3, 4);
        }
        for (int i = 0; i < l4sSizes.length; i++) {
            TpDI selChar = findHighestFillRateChar(l4sSizes[i]);
            l4sFontIds[i] = selChar.item2 % nFonts;
            l4sLettrs[i]  = selChar.item2 / nFonts;
            l4sFills[i]   = selChar.item1;
        }
        l4sSolutions = new Solution[nRenders];
        int index = 0;
        for (int k = 0; k < 4; k++) {
            int size = nRenders / 4;
            if (k < nRenders % 4) {
                size++;
            }
            if (size == 0) {
                break;
            }
            Solution[] tmp = findSolutionL4S(k, size);
            for (Solution sol : tmp) {
                l4sSolutions[index] = sol;
                index++;
            }
        }
    }

    Solution[] findSolutionL4S(int ct, int size) {
        Solution[] ret = null, tmp = null;
        int[] sizePs = new int[size];
        for (int i = 0; i < 3; i++) {
            sizePs[0] = i;
            if (size == 1) {
                tmp = searchSolutionL4S(ct, sizePs);
                if (ret == null || tmp[0].score > ret[0].score) {
                    ret = tmp;
                }
                continue;
            }
            for(int j = 0; j < 2; j++) {
                sizePs[1] = j;
                if (size == 2) {
                    tmp = searchSolutionL4S(ct, sizePs);
                    if (ret == null || tmp[0].score > ret[0].score) {
                        ret = tmp;
                    }
                    continue;
                }
                for (int k = 0; k < 2; k++) {
                    sizePs[2] = k;
                    tmp = searchSolutionL4S(ct, sizePs);
                    if (ret == null || tmp[0].score > ret[0].score) {
                        ret = tmp;
                    }
                }
            }
        }
        if (size == 4) {
            Arrays.fill(sizePs, 1);
            tmp = searchSolutionL4S(ct, sizePs);
            if (ret == null || tmp[0].score > ret[0].score) {
                ret = tmp;
            }
        }
        if (size > 1) {
            for (int i = 0; i < 5; i++) {
                for (int j = 0; j < sizePs.length - 1; j++) {
                    sizePs[j] = rand.nextInt(l4sSizes.length);
                }
                sizePs[sizePs.length - 1] = rand.nextInt(2);
                tmp = searchSolutionL4S(ct, sizePs);
                if (ret == null || tmp[0].score > ret[0].score) {
                    ret = tmp;
                }
            }
        }

        return ret;
    }

    Solution[] searchSolutionL4S(int ct, int[] sizePs) {
        Solution[] ret = new Solution[sizePs.length];
        short     med  = (short)l4sMeds[ct];
        short[][] img  = images[ct];
        short[][] tmp1 = new short[height][];
        short[][] tmp2 = new short[height][width];
        for (int i = 0; i < height; i++) {
            tmp1[i] = Arrays.copyOf(img[i], img[i].length);
        }
        for (int p = 0; p < sizePs.length; p++) {
            Solution sol = new Solution();
            ret[p] = sol;

            int pi = sizePs[p];
            int fontId   = l4sFontIds[pi];
            int fontSize = l4sSizes[pi];
            byte letter  = (byte)l4sLettrs[pi];

            Font  f  = fonts[fontId][fontSize];
            Gryph g  = f.gryphs[(int)letter];
            Gryph sp = f.gryphs[SPACE_CHAR_CODE];
            int minw = Math.min((int)g.width, (int)sp.width);
            int coln = (width + minw - 1) / minw;
            int rown = (height + f.height - 1) / f.height;

            sol.fontId = (byte)fontId;
            sol.fontSize = (byte)fontSize;
            sol.colorType = (byte)ct;
            sol.colorValue = med;
            sol.lines = new ByteList[rown];

            int rowi = 0;

            for (int y = 0; y < height; y += f.height) {
                ByteList bs = new ByteList(coln);
                sol.lines[rowi] = bs;
                rowi++;
                int x = 0;
                int ey = Math.min(y + f.height, height);
                while (x < width) {
                    int sm = 0;
                    int ex = Math.min(x + g.width, width);
                    int n = (ex - x) * (ey - y);
                    for (int ty = y; ty < ey; ty++) {
                        for (int tx = x; tx < ex; tx++) {
                            sm += (int)tmp1[ty][tx];
                        }
                    }
                    if ((double)sm < 0.5 * (double)(n * (int)med)) {
                        bs.add((byte)SPACE_CHAR_CODE);
                        x += sp.width;
                    } else {
                        for (int ty = y; ty < ey; ty++) {
                            for (int tx = x; tx < ex; tx++) {
                                if (tmp1[ty][tx] < med) {
                                    tmp1[ty][tx] = 0;
                                } else {
                                    tmp1[ty][tx] -= med;
                                }
                                if (g.exists(tx - x, ty - y)) {
                                    tmp2[ty][tx] += med;
                                }
                            }
                        }
                        bs.add(letter);
                        x += g.width;
                    }
                }
            }
            med = findMedian(tmp1, 3, 4);
        }

        TpDI tp = calcColorScore(img, tmp2);
        ret[0].score = tp.item2 > 0 ? tp.item1 / (double)tp.item2 : 0;

        return ret;
    }

    void tryL4S() {
        System.err.println("tryL4S");
        initL4S();
        if (l4sSolutions == null) {
            return;
        }
        int[] iL4S = new int[4];
        int[] sL4S = new int[4];
        int[] iGRD = new int[4];
        int[] sGRD = new int[4];
        int n1 = 0, n2 = 0;
        double sc1 = 0.0, sc2 = 0.0;
        for (int i = nRenders - 1; i >= 0; i--) {
            if (l4sSolutions[i] == null) {
                n1++;
            } else {
                sc1 += l4sSolutions[i].score;
                int c1 = l4sSolutions[i].colorType;
                iL4S[c1] = i;
                sL4S[c1]++;
            }
            if (grdSolution[i] == null) {
                n2++;
            } else {
                sc2 += grdSolution[i].score;
                int c2 = grdSolution[i].colorType;
                iGRD[c2] = i;
                sGRD[c2]++;
            }
        }
        if (n1 > 0 || n2 > 0) {
            if (sc1 > sc2) {
                System.err.println("swap L4S");
                grdSolution = l4sSolutions;
            }
            return;
        }
        int sw1 = 0, sw2 = 0;
        for (int col = 0; col < 4; col++) {
            if (sL4S[col] != sGRD[col]) {
                continue;
            }
            int df = Double.compare(l4sSolutions[iL4S[col]].score, grdSolution[iGRD[col]].score);
            if (df > 0) {
                sw1++;
                for (int i = 0; i < sL4S[col]; i++) {
                    grdSolution[iGRD[col] + i] = l4sSolutions[iL4S[col] + i];
                }
            } else if (df < 0) {
                sw2++;
                for (int i = 0; i < sL4S[col]; i++) {
                    l4sSolutions[iL4S[col] + i] = grdSolution[iGRD[col] + i];
                }
            }
        }
        sc1 = 0.0; sc2 = 0.0;
        for (int i = 0; i < nRenders; i++) {
            sc1 += l4sSolutions[i].score;
            sc2 += grdSolution[i].score;
        }
        if (sc1 > sc2) {
            System.err.printf("swap L4S [%d,%d] %n", sw1, sw2);
            grdSolution = l4sSolutions;
        }
    }

    Solution[] grdSolution = null;

    int[] solveGreedy(int iteration, int measuredMillis) {

        if (iteration == 0) {
            long time0 = System.currentTimeMillis();
            initGreedy();
            long time1 = System.currentTimeMillis();
            long diff = time1 - time0;
            if (diff < 0L) { diff = (long)measuredMillis; }
            makeGreedySolution(RUNNING_ON_SERVER ? measuredMillis - (int)diff : 90000);
            long time2 = System.currentTimeMillis();
            diff = time2 - time0;
            System.err.printf("remain: %d %n", measuredMillis - (int)diff);
            if (diff >= 0L && measuredMillis - (int)diff >= 2900) {
                tryL4S();
            }
        }

        if (grdSolution == null) {
            // avoid score -1.0
            return new int[0];
        }
        if (grdSolution.length <= iteration) {
            // avoid score -1.0
            return new int[0];
        }
        if (grdSolution[iteration] == null) {
            // avoid score -1.0
            return new int[0];
        }
        return grdSolution[iteration].get();
    }

    int[] grdMeds    = new int[4];
    int[] grdFontIds = new int[MAX_FONT_SIZE + 1];
    int[] grdLetters = new int[MAX_FONT_SIZE + 1];
    int[] grdMinY    = new int[MAX_FONT_SIZE + 1];
    int[] grdMaxY    = new int[MAX_FONT_SIZE + 1];
    int[][] grdSizeSet = null;
    int[] grdMaxs = null;
    TpDI[][] grdFillOrders = new TpDI[MAX_FONT_SIZE + 1][];
    void initGreedy() {
        for (int k = 0; k < 4; k++) {
            grdMeds[k] = (int)findMedian(images[k], 3, 4);
        }
        int[] offs = new int[MAX_FONT_SIZE + 1];
        int[] heis = new int[MAX_FONT_SIZE + 1];
        for (int size = MIN_FONT_SIZE; size <= MAX_FONT_SIZE; size++) {
            TpDI selChar = findHighestFillRateChar(size, false);
            grdFontIds[size] = selChar.item2 % nFonts;
            grdLetters[size]  = selChar.item2 / nFonts;
            Font f = fonts[grdFontIds[size]][size];
            grdMinY[size] = Integer.MAX_VALUE;
            grdFillOrders[size] = new TpDI[MAX_CHAR_CODE + 1];
            int to = 0, th = 0;
            for (int le = MIN_CHAR_CODE + 1; le <= MAX_CHAR_CODE; le++) {
                Gryph g = f.gryphs[le];
                int o = (int)g.offset;
                int h = (int)g.height;
                to += o;
                th += h;
                grdMinY[size] = Math.min(grdMinY[size], o);
                grdMaxY[size] = Math.max(grdMaxY[size], o + h - 1);
                double fill = (double)g.count() / (double)((int)f.height * (int)g.width);
                grdFillOrders[size][le] = new TpDI(fill, le);
            }
            Arrays.sort(grdFillOrders[size], MIN_CHAR_CODE + 1, MAX_CHAR_CODE + 1, new TpDIComparator());
            offs[size] = to / (MAX_CHAR_CODE - MIN_CHAR_CODE);
            heis[size] = th / (MAX_CHAR_CODE - MIN_CHAR_CODE);
        }
        int[] maxs = new int[5];
        int[] maxs1 = new int[5];
        int[][] idxs = new int[5][4];
        int ren = (nRenders + 3) / 4;
        for (int s0 = MIN_FONT_SIZE; s0 <= MAX_FONT_SIZE; s0++) {
            boolean[] p0 = new boolean[height];
            Font f0 = fonts[grdFontIds[s0]][s0];
            Gryph g0 = f0.gryphs[grdLetters[s0]];
            int cnt1 = 0;
            {
                int go = (int)g0.offset;
                int gh = go + (int)g0.height;
                for (int h = 0; h < height; h += (int)f0.height) {
                    for (int dh = go; dh < gh && h + gh < height; dh++) {
                        p0[h + dh] = true;
                        cnt1++;
                    }
                }
            }
            if (cnt1 > maxs[1]) {
                maxs1[3] = maxs1[2];
                maxs1[2] = maxs1[1];
                maxs1[1] = maxs[1];
                maxs[1] = cnt1;
                idxs[1][3] = idxs[1][2];
                idxs[1][2] = idxs[1][1];
                idxs[1][1] = idxs[1][0];
                idxs[1][0] = s0;
            } else if (cnt1 > maxs1[1]) {
                maxs1[3] = maxs1[2];
                maxs1[2] = maxs1[1];
                maxs1[1] = cnt1;
                idxs[1][3] = idxs[1][2];
                idxs[1][2] = idxs[1][1];
                idxs[1][1] = s0;
            } else if (cnt1 > maxs1[2]) {
                maxs1[3] = maxs1[2];
                maxs1[2] = cnt1;
                idxs[1][3] = idxs[1][2];
                idxs[1][2] = s0;
            } else if (cnt1 > maxs1[3]) {
                maxs1[3] = cnt1;
                idxs[1][3] = s0;
            }
            if (ren == 1) {
                continue;
            }
            for (int s1 = s0 + 1; s1 <= MAX_FONT_SIZE; s1 += 2) {
                boolean[] p1 = new boolean[height];
                Font f1 = fonts[grdFontIds[s1]][s1];
                Gryph g1 = f1.gryphs[grdLetters[s1]];
                int cnt2 = cnt1;
                {
                    int go = (int)g1.offset;
                    int gh = go + (int)g1.height;
                    for (int h = 0; h < height; h += (int)f1.height) {
                        for (int dh = go; dh < gh && h + gh < height; dh++) {
                            p1[h + dh] = true;
                            if (!p0[h + dh]) {
                                cnt2++;
                            }
                        }
                    }
                }
                if (cnt2 > maxs[2]) {
                    maxs1[0] = maxs[2];
                    maxs[2] = cnt2;
                    idxs[2][2] = idxs[2][0];
                    idxs[2][3] = idxs[2][1];
                    idxs[2][0] = s0;
                    idxs[2][1] = s1;
                } else if (cnt2 > maxs1[0]) {
                    maxs1[0] = cnt2;
                    idxs[2][2] = s0;
                    idxs[2][3] = s1;
                }
                if (ren <= 2) {
                    continue;
                }
                for (int s2 = s1 + 1; s2 <= MAX_FONT_SIZE; s2 += 3) {
                    boolean[] p2 = new boolean[height];
                    Font f2 = fonts[grdFontIds[s2]][s2];
                    Gryph g2 = f2.gryphs[grdLetters[s2]];
                    int cnt3 = cnt2;
                    {
                        int go = (int)g2.offset;
                        int gh = go + (int)g2.height;
                        for (int h = 0; h < height; h += (int)f2.height) {
                            for (int dh = go; dh < gh && h + gh < height; dh++) {
                                p2[h + dh] = true;
                                if (!(p0[h + dh] | p1[h + dh])) {
                                    cnt3++;
                                }
                            }
                        }
                    }
                    if (cnt3 > maxs[3]) {
                        maxs[3] = cnt3;
                        idxs[3][0] = s0;
                        idxs[3][1] = s1;
                        idxs[3][2] = s2;
                    }
                    if (ren <= 3) {
                        continue;
                    }
                    for (int s3 = s2 + 1; s3 <= MAX_FONT_SIZE; s3 += 4) {
                        Font f3 = fonts[grdFontIds[s3]][s3];
                        Gryph g3 = f3.gryphs[grdLetters[s3]];
                        int cnt4 = cnt3;
                        {
                            int go = (int)g3.offset;
                            int gh = go + (int)g3.height;
                            for (int h = 0; h < height; h += (int)f3.height) {
                                for (int dh = go; dh < gh && h + gh < height; dh++) {
                                    if (!(p0[h + dh] | p1[h + dh] | p2[h + dh])) {
                                        cnt4++;
                                    }
                                }
                            }
                        }
                        if (cnt4 > maxs[4]) {
                            maxs[4] = cnt4;
                            idxs[4][0] = s0;
                            idxs[4][1] = s1;
                            idxs[4][2] = s2;
                            idxs[4][3] = s3;
                        }
                    }
                }
            }
        }
        grdSizeSet = idxs;
        grdMaxs = maxs;
    }

    void makeLessThan4GreedySolution(long time0) {
        Solution[] sols = new Solution[4];
        long interval = 0;
    outerblock:
        {
            boolean[] flag = new boolean[MAX_FONT_SIZE + 1];
            for (int i = 0; i < 4; i++) {
                grdSizeSet[1][0] = grdSizeSet[1][i];
                if (grdSizeSet[1][0] < MIN_FONT_SIZE || MAX_FONT_SIZE < grdSizeSet[1][0]) {
                    System.err.printf("grdSizeSet[1][%d]: %d %n", i, grdSizeSet[1][0]);
                    continue;
                }
                flag[grdSizeSet[1][0]] = true;
                for (int col = 0; col < 4; col++) {
                    long time1 = System.currentTimeMillis();
                    if (time1 + interval > time0) {
                        break outerblock;
                    }
                    Solution[] tmp = makeOneGreedySolution(col, 1, 1);
                    long time2 = System.currentTimeMillis();
                    interval = Math.max(interval, time2 - time1);
                    if (sols[col] == null || tmp[0].score > sols[col].score) {
                        sols[col] = tmp[0];
                    }
                }
            }
            for (int size = MIN_FONT_SIZE; size < MIN_CHAR_CODE + 20; size++) {
                if (flag[size]) {
                    continue;
                }
                grdSizeSet[1][0] = size;
                for (int col = 0; col < 4; col++) {
                    long time1 = System.currentTimeMillis();
                    if (time1 + interval > time0) {
                        break outerblock;
                    }
                    Solution[] tmp = makeOneGreedySolution(col, 1, 1);
                    long time2 = System.currentTimeMillis();
                    interval = Math.max(interval, time2 - time1);
                    if (sols[col] == null || tmp[0].score > sols[col].score) {
                        sols[col] = tmp[0];
                    }
                }
            }
        }
        switch (nRenders) {
        case 3:
            {
                int c = 0, s = -1;
                for (int col = 0; col < 4; col++) {
                    if (sols[col] == null) {
                        c++;
                        s = col;
                    } else if (s < 0) {
                        s = col;
                    } else if (sols[col].score < sols[s].score) {
                        s = col;
                    }
                }
                if (c <= 1) {
                    int i = 0;
                    for (int col = 0; col < 4; col++) {
                        if (col != s) {
                            grdSolution[i] = sols[col];
                            i++;
                        }
                    }
                    break;
                }
            }
        case 2:
            {
                for (int c1 = 0; c1 < 4; c1++) {
                    if (sols[c1] == null) {
                        continue;
                    }
                    for (int c2 = c1 + 1; c2 < 4; c2++) {
                        if (sols[c2] == null) {
                            continue;
                        }
                        if (grdSolution[0] == null) {
                            grdSolution[0] = sols[c1];
                            grdSolution[1] = sols[c2];
                        } else {
                            double sc1 = grdSolution[0].score + grdSolution[1].score;
                            double sc2 = sols[c1].score + sols[c2].score;
                            if (sc2 > sc1) {
                                grdSolution[0] = sols[c1];
                                grdSolution[1] = sols[c2];
                            }
                        }
                    }
                }
                if (grdSolution[0] != null) {
                    break;
                }
            }
        case 1:
            {
                for (int col = 0; col < 4; col++) {
                    if (sols[col] == null) {
                        continue;
                    }
                    if (grdSolution[0] == null || sols[col].score > grdSolution[0].score) {
                        grdSolution[0] = sols[col];
                    }
                }
                break;
            }
        default:
            break;
        }
    }

    void makeBetween5to7GreedySolution(long time0) {
        Solution[][][] sols = new Solution[3][4][];
        long interval = 0;
        int index = 0;
        for (int col = 0; col < 4; col++) {
            int n = nRenders / 4;
            if (col < nRenders % 4) {
                n++;
            }
            long time1 = System.currentTimeMillis();
            if (time1 + interval > time0) {
                System.err.printf("interval: %d %n", interval);
                return;
            }
            Solution[] tmp = makeOneGreedySolution(col, n, n);
            long time2 = System.currentTimeMillis();
            interval = Math.max(interval, time2 - time1);
            if (grdSolution[index] == null || tmp[0].score > grdSolution[index].score) {
                sols[n][col] = tmp;
                for (int j = 0; j < n; j++) {
                    grdSolution[index] = tmp[j];
                    index++;
                }
            } else {
                index += n;
            }
        }
        int ones = 0;
        for (int col = 0; col < 4; col++) {
            if (sols[1][col] != null) {
                ones++;
                continue;
            }
            long time1 = System.currentTimeMillis();
            if (time1 + interval > time0) {
                System.err.printf("interval: %d [1-1] %n", interval);
                return;
            }
            sols[1][col] = makeOneGreedySolution(col, 1, 1);
            long time2 = System.currentTimeMillis();
            interval = Math.max(interval, time2 - time1);
            ones++;
        }
        boolean search = true;
        int twos = 0;
        for (int col = 0; col < 4; col++) {
            if (sols[2][col] != null) {
                twos++;
                continue;
            }
            long time1 = System.currentTimeMillis();
            if (time1 + interval > time0) {
                System.err.printf("interval: %d [2-2] %n", interval);
                search = false;
                break;
            }
            sols[2][col] = makeOneGreedySolution(col, 2, 2);
            long time2 = System.currentTimeMillis();
            interval = Math.max(interval, time2 - time1);
            twos++;
        }
        for (int col = 0; search && col < 4; col++) {
            long time1 = System.currentTimeMillis();
            if (time1 + interval > time0) {
                System.err.printf("interval: %d [1-2] %n", interval);
                search = false;
                break;
            }
            Solution[] tmp = makeOneGreedySolution(col, 1, 2);
            long time2 = System.currentTimeMillis();
            interval = Math.max(interval, time2 - time1);
            if (tmp[0].score > sols[2][col][0].score) {
                sols[2][col] = tmp;
            }
        }
        boolean[] flag = new boolean[MAX_FONT_SIZE + 1];
        flag[grdSizeSet[1][0]] = true;
        for (int k = 1; search && k < 4; k++) {
            grdSizeSet[1][0] = grdSizeSet[1][k];
            if (grdSizeSet[1][0] < MIN_FONT_SIZE || MAX_FONT_SIZE < grdSizeSet[1][0]) {
                System.err.printf("grdSizeSet[1][%d]: %d %n", k, grdSizeSet[1][0]);
                continue;
            }
            flag[grdSizeSet[1][0]] = true;
            for (int col = 0; col < 4; col++) {
                long time1 = System.currentTimeMillis();
                if (time1 + interval > time0) {
                    System.err.printf("interval: %d [1-1-k] %n", interval);
                    search = false;
                    break;
                }
                Solution[] tmp = makeOneGreedySolution(col, 1, 1);
                long time2 = System.currentTimeMillis();
                interval = Math.max(interval, time2 - time1);
                if (tmp[0].score > sols[1][col][0].score) {
                    sols[1][col] = tmp;
                }
            }
        }
        for (int k = 1; search && k < 4; k++) {
            grdSizeSet[1][0] = grdSizeSet[1][k];
            if (grdSizeSet[1][0] < MIN_FONT_SIZE || MAX_FONT_SIZE < grdSizeSet[1][0]) {
                continue;
            }
            for (int col = 0; col < 4; col++) {
                long time1 = System.currentTimeMillis();
                if (time1 + interval > time0) {
                    System.err.printf("interval: %d [1-2-k] %n", interval);
                    search = false;
                    break;
                }
                Solution[] tmp = makeOneGreedySolution(col, 1, 2);
                long time2 = System.currentTimeMillis();
                interval = Math.max(interval, time2 - time1);
                if (tmp[0].score > sols[2][col][0].score) {
                    sols[2][col] = tmp;
                }
            }
        }
    sub2sizeset:
        if (search) {
            grdSizeSet[2][0] = grdSizeSet[2][2];
            grdSizeSet[2][1] = grdSizeSet[2][3];
            if (grdSizeSet[2][0] < MIN_FONT_SIZE || MAX_FONT_SIZE < grdSizeSet[2][0]) {
                System.err.printf("grdSizeSet[2][2]: %d %n", grdSizeSet[2][0]);
                break sub2sizeset;
            }
            if (grdSizeSet[2][1] < MIN_FONT_SIZE || MAX_FONT_SIZE < grdSizeSet[2][1]) {
                System.err.printf("grdSizeSet[2][3]: %d %n", grdSizeSet[2][1]);
                break sub2sizeset;
            }
            for (int col = 0; col < 4; col++) {
                long time1 = System.currentTimeMillis();
                if (time1 + interval > time0) {
                    System.err.printf("interval: %d [2-2-k] %n", interval);
                    search = false;
                    break;
                }
                Solution[] tmp = makeOneGreedySolution(col, 2, 2);
                long time2 = System.currentTimeMillis();
                interval = Math.max(interval, time2 - time1);
                if (tmp[0].score > sols[2][col][0].score) {
                    sols[2][col] = tmp;
                }
            }
        }
        for (int size = MIN_FONT_SIZE; search && size < MIN_FONT_SIZE + 20; size++) {
            if (flag[size]) {
                continue;
            }
            grdSizeSet[1][0] = size;
            for (int col = 0; col < 4; col++) {
                long time1 = System.currentTimeMillis();
                if (time1 + interval > time0) {
                    System.err.printf("interval: %d [1-1-size] %n", interval);
                    search = false;
                    break;
                }
                Solution[] tmp = makeOneGreedySolution(col, 1, 1);
                long time2 = System.currentTimeMillis();
                interval = Math.max(interval, time2 - time1);
                if (tmp[0].score > sols[1][col][0].score) {
                    sols[1][col] = tmp;
                }
            }
            for (int col = 0; search && col < 4; col++) {
                long time1 = System.currentTimeMillis();
                if (time1 + interval > time0) {
                    System.err.printf("interval: %d [1-2-size] %n", interval);
                    search = false;
                    break;
                }
                Solution[] tmp = makeOneGreedySolution(col, 1, 2);
                long time2 = System.currentTimeMillis();
                interval = Math.max(interval, time2 - time1);
                if (tmp[0].score > sols[2][col][0].score) {
                    sols[2][col] = tmp;
                }
            }
        }

        switch (nRenders) {
        case 7:
            {
                if (ones < 2 || twos < 4){
                    break;
                }
                int s = -1;
                double bsc = 0.0;
                for (int c1 = 0; c1 < 4; c1++) {
                    if (sols[1][c1] == null) {
                        continue;
                    }
                    double tsc = sols[1][c1][0].score;
                    for (int c2 = 0; c2 < 4; c2++) {
                        if (c1 == c2) {
                            continue;
                        }
                        tsc += sols[2][c2][0].score;
                    }
                    if (tsc > bsc) {
                        bsc = tsc;
                        s = c1;
                    }
                }
                if (s >= 0) {
                    index = 0;
                    for (int col = 0; col < 4; col++) {
                        if (s == col) {
                            grdSolution[index] = sols[1][col][0];
                            index++;
                        } else {
                            grdSolution[index] = sols[2][col][0];
                            index++;
                            grdSolution[index] = sols[2][col][1];
                            index++;
                        }
                    }
                }
                break;
            }
        case 6:
            {
                if (ones < 3 || twos < 3) {
                    break;
                }
                double bsc = 0.0;
                int s1 = -1, s2 = -1;
                for (int c1 = 0; c1 < 4; c1++) {
                    if (sols[2][c1] == null) {
                        continue;
                    }
                    for (int c2 = c1 + 1; c2 < 4; c2++) {
                        if (sols[2][c2] == null) {
                            continue;
                        }
                        double tsc = 0.0;
                        for (int c3 = 0; c3 < 4; c3++) {
                            if (c1 == c3 || c2 == c3) {
                                tsc += sols[2][c3][0].score;
                            } else if (sols[1][c3] == null) {
                                tsc = -1.0;
                                break;
                            } else {
                                tsc += sols[1][c3][0].score;
                            }
                        }
                        if (tsc > bsc) {
                            bsc = tsc;
                            s1 = c1;
                            s2 = c2;
                        }
                    }
                }
                if (s1 >= 0 && s2 >= 0) {
                    index = 0;
                    for (int col = 0; col < 4; col++) {
                        if (s1 == col || s2 == col) {
                            grdSolution[index] = sols[2][col][0];
                            index++;
                            grdSolution[index] = sols[2][col][1];
                            index++;
                        } else {
                            grdSolution[index] = sols[1][col][0];
                            index++;
                        }
                    }
                }
                break;
            }
        case 5:
            {
                if (ones < 4 || twos < 2) {
                    break;
                }
                int s = -1;
                double bsc = 0.0;
                for (int c1 = 0; c1 < 4; c1++) {
                    if (sols[2][c1] == null) {
                        continue;
                    }
                    double tsc = sols[2][c1][0].score;
                    for (int c2 = 0; c2 < 4; c2++) {
                        if (c1 == c2) {
                            continue;
                        }
                        tsc += sols[1][c2][0].score;
                    }
                    if(tsc > bsc) {
                        bsc = tsc;
                        s = c1;
                    }
                }
                if (s >= 0) {
                    index = 0;
                    for (int col = 0; col < 4; col++) {
                        if (s == col) {
                            grdSolution[index] = sols[2][col][0];
                            index++;
                            grdSolution[index] = sols[2][col][1];
                            index++;
                        } else {
                            grdSolution[index] = sols[1][col][0];
                            index++;
                        }
                    }
                }
                break;
            }
        default:
            break;
        }

        System.err.printf("interval: %d [finished] %n", interval);
    }

    void makeGreedySolution(int measuredMillis) {
        long time0 = System.currentTimeMillis() + (long)measuredMillis - 300L;
        long interval = 0;
        grdSolution = new Solution[nRenders];
        if (nRenders < 4) {
            makeLessThan4GreedySolution(time0);
            return;
        }
        if (5 <= nRenders && nRenders <= 7) {
            makeBetween5to7GreedySolution(time0);
            return;
        }
        int[] ws = Arrays.copyOf(grdMeds, grdMeds.length);
        for (int i = 0; i < ws.length; i++) {
            ws[i] = ws[i] * 10 + i;
        }
        Arrays.sort(ws);
        for (int k = 0; k < 4; k++) {
            int index = 0;
            for (int i = 0; i < 4; i++) {
                int n = nRenders / 4;
                if (i < nRenders % 4) {
                    n++;
                }
                int e = n - k;
                if (e <= 0) {
                    index += n;
                    continue;
                }
                int col = ws[3 - i] % 10;
                long time1 = System.currentTimeMillis();
                if (time1 + interval > time0) {
                    System.err.printf("interval: %d %n", interval);
                    return;
                }
                Solution[] tmp = makeOneGreedySolution(col, e, n);
                long time2 = System.currentTimeMillis();
                interval = Math.max(interval, time2 - time1);
                if (grdSolution[index] == null || tmp[0].score > grdSolution[index].score) {
                    for (int j = 0; j < n; j++) {
                        grdSolution[index] = tmp[j];
                        index++;
                    }
                } else {
                    index += n;
                }
            }
        }
        boolean[] flag = new boolean[MAX_FONT_SIZE + 1];
        flag[grdSizeSet[1][0]] = true;
        for (int k = 1; k < 4; k++) {
            grdSizeSet[1][0] = grdSizeSet[1][k];
            if (grdSizeSet[1][0] < MIN_FONT_SIZE || MAX_FONT_SIZE < grdSizeSet[1][0]) {
                System.err.printf("grdSizeSet[1][%d]: %d %n", k, grdSizeSet[1][k]);
                continue;
            }
            flag[grdSizeSet[1][0]] = true;
            int index = 0;
            for (int i = 0; i < 4; i++) {
                int n = nRenders / 4;
                if (i < nRenders % 4) {
                    n++;
                }
                int col = ws[3 - i] % 10;
                long time1 = System.currentTimeMillis();
                if (time1 + interval > time0) {
                    System.err.printf("interval: %d %n", interval);
                    return;
                }
                Solution[] tmp = makeOneGreedySolution(col, 1, n);
                long time2 = System.currentTimeMillis();
                interval = Math.max(interval, time2 - time1);
                if (grdSolution[index] == null || tmp[0].score > grdSolution[index].score) {
                    for (int j = 0; j < n; j++) {
                        grdSolution[index] = tmp[j];
                        index++;
                    }
                } else {
                    index += n;
                }

            }
        }
    sub2sizeset:
        {
            grdSizeSet[2][0] = grdSizeSet[2][2];
            grdSizeSet[2][1] = grdSizeSet[2][3];
            if (grdSizeSet[2][0] < MIN_FONT_SIZE || MAX_FONT_SIZE < grdSizeSet[2][0]) {
                System.err.printf("grdSizeSet[2][2]: %d %n", grdSizeSet[2][0]);
                break sub2sizeset;
            }
            if (grdSizeSet[2][1] < MIN_FONT_SIZE || MAX_FONT_SIZE < grdSizeSet[2][1]) {
                System.err.printf("grdSizeSet[2][3]: %d %n", grdSizeSet[2][1]);
                break sub2sizeset;
            }
            int index = 0;
            for (int i = 0; i < 4; i++) {
                int n = nRenders / 4;
                if (i < nRenders % 4) {
                    n++;
                }
                int col = ws[3 - i] % 10;
                long time1 = System.currentTimeMillis();
                if (time1 + interval > time0) {
                    System.err.printf("interval: %d %n", interval);
                    return;
                }
                Solution[] tmp = makeOneGreedySolution(col, 2, n);
                long time2 = System.currentTimeMillis();
                interval = Math.max(interval, time2 - time1);
                if (grdSolution[index] == null || tmp[0].score > grdSolution[index].score) {
                    for (int j = 0; j < n; j++) {
                        grdSolution[index] = tmp[j];
                        index++;
                    }
                } else {
                    index += n;
                }
            }
        }
        for (int size = MIN_FONT_SIZE; size < MIN_FONT_SIZE + 20; size++) {
            if (flag[size]) {
                continue;
            }
            grdSizeSet[1][0] = size;
            int index = 0;
            for (int i = 0; i < 4; i++) {
                int n = nRenders / 4;
                if (i < nRenders % 4) {
                    n++;
                }
                int col = ws[3 - i] % 10;
                long time1 = System.currentTimeMillis();
                if (time1 + interval > time0) {
                    System.err.printf("interval: %d [size] %n", interval);
                    return;
                }
                Solution[] tmp = makeOneGreedySolution(col, 1, n);
                long time2 = System.currentTimeMillis();
                interval = Math.max(interval, time2 - time1);
                if (grdSolution[index] == null || tmp[0].score > grdSolution[index].score) {
                    for (int j = 0; j < n; j++) {
                        grdSolution[index] = tmp[j];
                        index++;
                    }
                } else {
                    index += n;
                }

            }
        }
        System.err.printf("interval: %d [finished] %n", interval);
    }

    Solution[] makeOneGreedySolution(int col, int n, int len) {
        Solution[] ret = new Solution[len];
        short[][] tmp1 = images[col];
        short[][] tmp2 = new short[height][width];

        for (int it = 0; it < len; it++) {
            Solution sol = new Solution();
            ret[it] = sol;

            int s = it % n;
            if ((it / n) % 2 == 0) {
                s = (n - 1) - s;
            }

            int fontSize = grdSizeSet[n][s];
            int fontId = grdFontIds[fontSize];
            byte letter = (byte)grdLetters[fontSize];

            Font f   = fonts[fontId][fontSize];
            Gryph g  = f.gryphs[letter];
            Gryph sp = f.gryphs[SPACE_CHAR_CODE];

            int www = (width + (int)f.minWidth - 1) / (int)f.minWidth;
            int hhh = (height + (int)f.height - 1) / (int)f.height;

            short value = 0;
            {
                int minY = grdMinY[fontSize];
                int maxY = grdMaxY[fontSize];
                short[] xs = new short[(hhh * (maxY - minY + 1)) * width];
                int index = 0;
                for (int y = 0; y < height; y += (int)f.height) {
                    int ey = Math.min(maxY + 1, height - y);
                    for (int dy = minY; dy < ey; dy++) {
                        for (int x = 0; x < width; x++) {
                            xs[index] = (short)Math.max(0, (int)tmp1[y + dy][x] - (int)tmp2[y + dy][x]);
                            index++;
                        }
                    }
                }
                Arrays.sort(xs);
                index = 0;
                while (index < xs.length && xs[index] == 0) {
                    index++;
                }
                index = (xs.length - index) * 3 / 4 + index;
                if (index < xs.length) {
                    value = xs[index];
                } else {
                    value = 10;
                }
            }

            short erase = (short)(-value);

            sol.fontId = (byte)fontId;
            sol.fontSize = (byte)fontSize;
            sol.colorType = (byte)col;
            sol.colorValue = value;

            List<ByteList> lines = new ArrayList<ByteList>(hhh);

            for (int y = 0; y < height; y += (int)f.height) {
                ByteList line = new ByteList(www, 20);
                lines.add(line);
                for (int x = 0; x < width; ) {
                    Gryph tg = g;
                    byte le = letter;
                    g.print(tmp2, x, y, value);
                    double s1 = calcPartialScore(tmp1, tmp2, x, y, (int)g.width, (int)f.height);
                    g.print(tmp2, x, y, erase);

                    sp.print(tmp2, x, y, value);
                    double s2 = calcPartialScore(tmp1, tmp2, x, y, (int)sp.width, (int)f.height);
                    sp.print(tmp2, x, y, erase);
                    if (s2 > s1) {
                        tg = sp;
                        le = SPACE_CHAR_CODE;
                        s1 = s2;
                    }

                    for (int i = MIN_CHAR_CODE + 1; i <= MAX_CHAR_CODE; i += 6) {
                        int ch = grdFillOrders[fontSize][i].item2;
                        Gryph chg = f.gryphs[ch];
                        chg.print(tmp2, x, y, value);
                        s2 = calcPartialScore(tmp1, tmp2, x, y, (int)chg.width, (int)f.height);
                        chg.print(tmp2, x, y, erase);
                        if (s2 > s1) {
                            tg = chg;
                            le = (byte)ch;
                            s1 = s2;
                        }
                    }

                    tg.print(tmp2, x, y, value);
                    x += (int)tg.width;
                    line.add(le);

                }
            }

            sol.lines = lines.toArray(new ByteList[0]);
        }

        TpDI csc = calcColorScore(tmp1, tmp2);
        ret[0].score = csc.item2 > 0 ? csc.item1 / (double)csc.item2 : 0;
        ret[0].layer = tmp2;
        return ret;
    }

}

class Gryph {
    short width;
    byte column, offset, height;
    int[] bitmask;
    Gryph(int w, int h, int[] bm) {
        width = (short)w;
        column = (byte)(bm.length / h);
        offset = (byte)h;
        for (int i = 0; i < bm.length; i++) {
            if (bm[i] != 0) {
                offset = (byte)(i / (int)column);
                break;
            }
        }
        height = (byte)h;
        for (int i = bm.length - 1; i >= 0; i--) {
            if (bm[i] != 0) {
                height = (byte)(i / (int)column + 1);
                break;
            }
        }
        height -= (byte)offset;
        // bitmask = Arrays.copyOfRange(bm, (int)offset * (int)column, (int)(offset + height) * (int)column);
        bitmask = bm;
    }
    boolean exists(int x, int y) {
        return (bitmask[(int)column * y + (x >> 5)] & (1 << (x & 31))) != 0;
    }
    int count() {
        int ret = 0;
        for (int m : bitmask) {
            ret += Integer.bitCount(m);
        }
        return ret;
    }
    void print(short[][] layer, int x, int y, short v) {
        for (int dy = (int)offset; dy < (int)offset + (int)height && y + dy < layer.length; dy++) {
            for (int dx = 0; dx < width && x + dx < layer[0].length; dx++) {
                if (exists(dx, dy)) {
                    layer[y + dy][x + dx] += v;
                }
            }
        }
    }
}

class Font {
    short height;
    byte zeroWidth = 0;
    short minWidth = Short.MAX_VALUE;
    short maxWidth = Short.MIN_VALUE;
    Gryph[] gryphs = new Gryph[TypesetterArt.MAX_CHAR_CODE + 1];
    void updateWidth(int w) {
        if (w == 0) {
            zeroWidth++;
        } else {
            minWidth = (short)Math.min((int)minWidth, w);
            maxWidth = (short)Math.max((int)maxWidth, w);
        }
    }
    boolean isProportional() {
        return minWidth != maxWidth;
    }
}

class IntList {
    int[] buf;
    int alloc;
    int index = 0;
    IntList(int cap, int a) {
        buf = new int[cap];
        alloc = a;
    }
    IntList(int cap) {
        this(cap, 1000);
    }
    IntList() {
        this(1000, 1000);
    }
    void add(int value) {
        if (index >= buf.length) {
            buf = Arrays.copyOf(buf, buf.length + alloc);
        }
        buf[index] = value;
        index++;
    }
    int size() {
        return index;
    }
    int[] toArray() {
        return Arrays.copyOf(buf, index);
    }
    void release() {
        buf = null;
    }
}

class ByteList {
    byte[] buf;
    int alloc;
    int index = 0;
    ByteList(int cap, int a) {
        buf = new byte[cap];
        alloc = a;
    }
    ByteList(int cap) {
        this(cap, 1000);
    }
    ByteList() {
        this(1000, 1000);
    }
    void add(byte value) {
        if (index >= buf.length) {
            buf = Arrays.copyOf(buf, buf.length + alloc);
        }
        buf[index] = value;
        index++;
    }
    int size() {
        return index;
    }
    byte[] toArray() {
        return Arrays.copyOf(buf, index);
    }
    void release() {
        buf = null;
    }
}

class RandomEx extends Random {
    RandomEx(long seed) {
        super(seed);
        nextInt();
    }
    int nextInt(int min, int max) {
        return nextInt(max - min) + min;
    }
}

class TpDI {
    double item1;
    int item2;
    TpDI(double i1, int i2) {
        item1 = i1;
        item2 = i2;
    }
}

class TpDIComparator implements Comparator<TpDI> {
    public int compare(TpDI o1, TpDI o2) {
        return Double.compare(o1.item1, o2.item1);
    }
}

class Solution {
    byte fontId;
    byte fontSize;
    byte colorType;
    short colorValue;
    ByteList[] lines;
    short[][] layer;
    double score;
    int[] get() {
        int size = 4 + lines.length;
        for (ByteList line : lines) {
            size += line.size();
        }
        IntList ret = new IntList(size);
        ret.add((int)fontId);
        ret.add((int)fontSize);
        ret.add((int)colorType);
        ret.add((int)colorValue);
        for (ByteList line : lines) {
            for (int i = 0; i < line.size(); i++) {
                ret.add((int)line.buf[i]);
            }
            ret.add(TypesetterArt.LINE_CHAR_CODE);
        }
        return ret.buf;
    }
}