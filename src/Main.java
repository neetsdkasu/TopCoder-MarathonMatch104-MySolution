import java.io.*;
import java.util.*;

public class Main {

    static Stopwatch sw = new Stopwatch();

    static int callInit(BufferedReader in, TypesetterArt typesetterArt) throws Exception {

        int nRenders = Integer.parseInt(in.readLine());
        int nFonts = Integer.parseInt(in.readLine());
        int precision = Integer.parseInt(in.readLine());
        int width = Integer.parseInt(in.readLine());
        int height = Integer.parseInt(in.readLine());
        int _imageSize = Integer.parseInt(in.readLine());
        int[] image = new int[_imageSize];
        for (int _idx = 0; _idx < _imageSize; _idx++) {
            image[_idx] = Integer.parseInt(in.readLine());
        }

        sw.start();
        int _result = typesetterArt.init(nRenders, nFonts, precision, width, height, image);
        sw.stop();

        System.out.println(_result);
        System.out.flush();

        return nRenders;
    }

    static int callNextGlyph(BufferedReader in, TypesetterArt typesetterArt) throws Exception {

        int width = Integer.parseInt(in.readLine());
        int height = Integer.parseInt(in.readLine());
        int _bitmaskSize = Integer.parseInt(in.readLine());
        int[] bitmask = new int[_bitmaskSize];
        for (int _idx = 0; _idx < _bitmaskSize; _idx++) {
            bitmask[_idx] = Integer.parseInt(in.readLine());
        }
        int measuredMillis = Integer.parseInt(in.readLine());

        sw.start();
        int[] _result = typesetterArt.nextGlyph(width, height, bitmask, measuredMillis);
        sw.stop();

        if (_result.length == 3) {
            System.out.printf("%d %d %d%n",
                _result[0],
                _result[1],
                _result[2]
            );
        } else {
            System.out.println();
        }

        return _result.length;
    }

    static int callRender(BufferedReader in, TypesetterArt typesetterArt, int iteration) throws Exception {

        int measuredMillis = Integer.parseInt(in.readLine());

        sw.start();
        int[] _result = typesetterArt.render(iteration, measuredMillis);
        sw.stop();

        System.out.println(_result.length);
        for (int _it : _result) {
            System.out.println(_it);
        }
        System.out.flush();

        return 0;
    }

    public static void main(String[] args) throws Exception {

        TypesetterArt.RUNNING_ON_SERVER = false;

        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));

        sw.start();
        TypesetterArt typesetterArt = new TypesetterArt();
        sw.stop();
        long cEndTime = sw.getTotalTime();
        System.err.printf("construct: %d ms%n", cEndTime);

        int nRenders = callInit(in, typesetterArt);
        long iEndTime = sw.getTotalTime();
        System.err.printf("init: %d ms%n", iEndTime - cEndTime);

        while (callNextGlyph(in, typesetterArt) > 0) {
            // no code
        }
        long nEndTime = sw.getTotalTime();
        System.err.printf("nextGlyph: %d ms%n", nEndTime - iEndTime);

        for (int i = 0; i < nRenders; i++) {
            callRender(in, typesetterArt, i);
        }
        long rEndTime = sw.getTotalTime();
        System.err.printf("render: %d ms%n", rEndTime - nEndTime);

        System.err.printf("total time: %d ms%n", rEndTime);
    }

}

class Stopwatch {
    long totalTime = 0, startTime;
    public void start() {
        startTime = System.currentTimeMillis();
    }
    public void stop() {
        long stopTime = System.currentTimeMillis();
        totalTime += stopTime - startTime;
    }
    public long getTotalTime() {
        return totalTime;
    }
}