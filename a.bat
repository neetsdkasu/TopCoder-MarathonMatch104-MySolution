@echo off
if "%~1"=="" goto defaultseed
if "%~2"=="-save" goto savemode
java -jar tester.jar -exec "java -cp classes Main" -seed %*
@GOTO finally
:savemode
java -jar tester.jar -exec "java -cp classes Main" -save %~1.png -seed %1 %3 %4 %5 %6 %7 %8 %9
@GOTO finally
:defaultseed
java -jar tester.jar -exec "java -cp classes Main" -save 1.png -seed 1
@GOTO finally
:finally
