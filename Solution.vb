Option Explicit On
Option Strict   On
Option Compare  Binary
Option Infer    On

Imports System
Imports System.Collections.Generic
Imports TpDI = System.Tuple(Of Double, Integer)

#Const OffGryph = False

Public Class TypesetterArt
    Dim startTime As Integer, limitTime As Integer
    Dim rand As New Random(19831983)

    Const LineCharCode   As Integer = 10
    Const MinCharCode    As Integer = 32
    Const MaxCharCode    As Integer = 126
    Const MinFontSize    As Integer = 8
    Const MaxFontSize    As Integer = 72
    Const MaxColorValue  As Integer = &HFF

    Dim nRenders, nFonts, precision As Integer
    Dim width, height As Integer
    Dim images(3)(,) As Byte
    Dim layers(3)(,) As Short


    Public Function init(nRenders As Integer, nFonts As Integer, precision As Integer, width As Integer, height As Integer, image() As Integer) As Integer
        Me.nRenders  = nRenders
        Me.nFonts    = nFonts
        Me.precision = precision
        Me.width     = width
        Me.height    = height

        Dim index As Integer = 0
        For k As Integer = 0 To 3
            ReDim layers(k)(height - 1, width - 1)
            Dim tmp(height - 1, width - 1) As Byte
            For y As Integer = 0 To height - 1
                For x As Integer = 0 To width - 1
                    tmp(y, x) = CByte(Math.Min(MaxColorValue, image(index)))
                    index += 1
                Next x
            Next y
            images(k) = tmp
        Next k

        Dim score As Double = CalcScore(layers)
        Console.Error.WriteLine("score is {0}", score)

        init = 0
    End Function

    Class Gryph
        Public Width As Byte
        Public Offset As Byte
        Public Bitmask() As Integer
        Sub New(w As Integer, bm() As Integer)
            Me.Width = CByte(w)
            Me.Offset = CByte((w + 31) >> 5)
            Me.Bitmask = bm
        End Sub
        Public Function Exists(y As Integer, x As Integer) As Boolean
            Exists = (Bitmask(CInt(Offset) * y + (x >> 5)) And (1 << (x And 31))) <> 0
        End Function
    End Class

    Class Font
        Public Height As Byte
        Public ZeroWidth As Byte = 0
        Public MinWidth As Byte = Byte.MaxValue
        Public MaxWidth As Byte = Byte.MinValue
        Public Gryphs(MaxCharCode) As Gryph
        Public Sub UpdateWidth(w As Integer)
            If w = 0 Then
                ZeroWidth += CByte(1)
            Else
                MinWidth = Math.Min(MinWidth, CByte(w))
                MaxWidth = Math.Max(MaxWidth, CByte(w))
            End If
        End Sub
    End Class

    Dim ngFontId   As Integer = 0
    Dim ngFontSize As Integer = 0
    Dim ngLetterId As Integer = 0
    Dim fonts(,) As Font

    Sub InitNextGlyph()
        ngFontId = 0
        ngFontSize = MinFontSize
        ngLetterId = MinCharCode
        ReDim fonts(nFonts - 1, MaxFontSize)
        For id As Integer = 0 To nFonts - 1
            For size As Integer = MinFontSize To MaxFontSize
                fonts(id, size) = New Font()
            Next size
        Next id
    End Sub

    Public Function nextGlyph(width As Integer, height As Integer, bitmask() As Integer, measuredMillis As Integer) As Integer()

#If OffGryph Then
        nextGlyph = New Integer(){}
        If ngFontId = 0 Then Exit Function
#End If

        If ngFontSize = 0 Then
            ' first time
            InitNextGlyph()
        Else
            ' save font data
            Dim f As Font = fonts(ngFontId, ngFontSize)
            f.Height = CByte(height)
            f.Gryphs(ngLetterId) = New Gryph(width, bitmask)
            f.UpdateWidth(width)

            ' update
            ngLetterId += 1
            If ngLetterId > MaxCharCode Then
                ngLetterId = MinCharCode
                ngFontSize += 1
                If ngFontSize > MaxFontSize Then
                    ngFontSize = MinFontSize
                    ngFontId += 1
                End If
            End If
        End If

        If ngFontId < nFonts Then
            nextGlyph = New Integer(){ ngLetterId, ngFontId, ngFontSize }
        Else
            nextGlyph = New Integer(){}
            Dim zc As Integer = 0
            Dim pc As Integer = 0
            Dim fc As Integer = 0
            For id As Integer = 0 To nFonts - 1
                For size As Integer = MinFontSize To MaxFontSize
                    Dim f As Font = fonts(id, size)
                    If f.ZeroWidth > 0 Then
                        zc += 1
                    End If
                    If f.MinWidth = f.MaxWidth Then
                        fc += 1
                    Else
                        pc += 1
                    End If
                Next size
            Next id
            Console.Error.WriteLine("zeros: {0}, fixed: {1}, props: {2}", zc, fc, pc)
        End If

    End Function

    Public Function render(iteration As Integer, measuredMillis As Integer) As Integer()
        Dim ret As New List(Of Integer)()

        Dim fontId     As Integer = rand.Next(nFonts)
        Dim fontSize   As Integer = rand.Next(MinFontSize, MaxFontSize + 1)
        Dim colorType  As Integer = rand.Next(4)
        Dim colorValue As Short   = CShort(rand.Next(MaxColorValue + 1))

        ret.Add(fontId)
        ret.Add(fontSize)
        ret.Add(colorType)
        ret.Add(CInt(colorValue))
        Dim f As Font = fonts(fontId, fontSize)
        Dim tmp(,) As Short = layers(colorType)
        Dim offY As Integer = 0
        For i As Integer = 0 To 400
            Dim h As Integer = Math.Min(f.Height, height - offY) - 1
            Dim offX As Integer = 0
            For j As Integer = 0 To 400
                Dim charCode As Integer = rand.Next(MinCharCode, MaxCharCode + 1)
                Dim g As Gryph = f.Gryphs(charCode)
                ret.Add(charCode)
                Dim w As Integer = Math.Min(g.Width, width - offX) - 1
                For y As Integer = 0 To h
                    For x As Integer = 0 To w
                        If g.Exists(y, x) Then
                            tmp(y + offY, x + offX) += colorValue
                        End If
                    Next x
                Next y
                offX += g.Width
                If offX >= width Then
                    Exit For
                End If
            Next j
            ret.Add(LineCharCode)
            offY += f.Height
            If offY >= height Then
                Exit For
            End If
        Next i

        Dim score As Double = CalcScore(layers)
        Console.Error.WriteLine("score is {0}", score)

        render = ret.ToArray()

    End Function

    Function popCount(w As Integer) As Integer
        Dim v As Long = CLng(w)
        v = (v And &H55555555&) + ((v >> 1) And &H55555555&)
        v = (v And &H33333333&) + ((v >> 2) And &H33333333&)
        v = (v And &H0F0F0F0F&) + ((v >> 4) And &H0F0F0F0F&)
        v = (v And &H00FF00FF&) + ((v >> 8) And &H00FF00FF&)
        v = (v And &H0000FFFF&) + ((v >> 16) And &H0000FFFF&)
        popCount = CInt(v)
    End Function

    Function CalcScore(target()(,) As Short) As Double
        Dim score As Double = 0.0
        Dim dv As Integer = 0
        For k As Integer = 0 To 3
            Dim p As TpDI = CalcColorScore(images(k), target(k))
            score += p.Item1
            dv += p.Item2
        Next k
        CalcScore = If(dv > 0, score / CDbl(dv), 0.0) * 1000000.0
    End Function

    Function CalcColorScore(tmp1(,) As Byte, tmp2(,) As Short) As TpDI
        Dim score As Double = 0.0
        Dim dv As Integer = 0
        For y As Integer = 0 To height - 1 Step precision
            For x As Integer = 0 To width - 1 Step precision
                Dim sum1 As Integer = 0
                Dim sum2 As Integer = 0
                Dim n As Integer = 0
                For ty As Integer = y To Math.Min(y + precision, height) - 1
                    For tx As Integer = x To Math.Min(x + precision, width) - 1
                        sum1 += CInt(tmp1(ty, tx))
                        sum2 += Math.Min(MaxColorValue, CInt(tmp2(ty, tx)))
                        n += 1
                    Next tx
                Next ty
                If n > 0 Then
                    Dim e1 As Double = CDbl(sum1) / CDbl(n * MaxColorValue)
                    Dim e2 As Double = CDbl(sum2) / CDbl(n * MaxColorValue)
                    Dim s As Double = 1.0 - Math.Abs(e1 - e2)
                    score += s * s
                    dv += 1
                End If
            Next x
        Next y
        CalcColorScore = New TpDI(score, dv)
    End Function

End Class