Option Explicit On
Option Strict   On
Option Compare  Binary
Option Infer    On
Imports System.Diagnostics
Imports Console = System.Console

Public Module Main

    Dim sw As Stopwatch = Nothing

    Public Sub Main()
        Try
            sw = New Stopwatch()

            sw.Start()
            Dim _typesetterArt As New TypesetterArt()
            sw.Stop()
            Dim cEndTime As Long = sw.ElapsedMilliseconds
            Console.Error.WriteLine("construct: {0} ms", cEndTime)

            Dim nRenders As Integer = CallInit(_typesetterArt)
            Dim iEndTime As Long = sw.ElapsedMilliseconds
            Console.Error.WriteLine("init: {0} ms", iEndTime - cEndTime)

            Do While CallNextGlyph(_typesetterArt) <> 0
            Loop
            Dim nEndTime As Long = sw.ElapsedMilliseconds
            Console.Error.WriteLine("nextGlyph: {0} ms", nEndTime - iEndTime)

            For i As Integer = 0 To nRenders - 1
                CallRender(_typesetterArt, i)
            Next i
            Dim rEndTime As Long = sw.ElapsedMilliseconds
            Console.Error.WriteLine("render: {0} ms", rEndTime - nEndTime)

            Console.Error.WriteLine("total time: {0} ms", rEndTime)

        Catch Ex As Exception
            Console.Error.WriteLine(ex)
        End Try
    End Sub

    Function CallInit(_typesetterArt As TypesetterArt) As Integer
        Dim nRenders As Integer = CInt(Console.ReadLine())
        Dim nFonts As Integer = CInt(Console.ReadLine())
        Dim precision As Integer = CInt(Console.ReadLine())
        Dim width As Integer = CInt(Console.ReadLine())
        Dim height As Integer = CInt(Console.ReadLine())
        Dim _imageSize As Integer = CInt(Console.ReadLine()) - 1
        Dim image(_imageSize) As Integer
        For _idx As Integer = 0 To _imageSize
            image(_idx) = CInt(Console.ReadLine())
        Next _idx

        sw.Start()
        Dim _result As Integer = _typesetterArt.init(nRenders, nFonts, precision, width, height, image)
        sw.Stop()

        Console.WriteLine(_result)
        Console.Out.Flush()

        CallInit = nRenders
    End Function

    Function CallNextGlyph(_typesetterArt As TypesetterArt) As Integer
        Dim width As Integer = CInt(Console.ReadLine())
        Dim height As Integer = CInt(Console.ReadLine())
        Dim _bitmaskSize As Integer = CInt(Console.ReadLine()) - 1
        Dim bitmask(_bitmaskSize) As Integer
        For _idx As Integer = 0 To _bitmaskSize
            bitmask(_idx) = CInt(Console.ReadLine())
        Next _idx
        Dim measuredMillis As Integer = CInt(Console.ReadLine())

        sw.Start()
        Dim _result() As Integer = _typesetterArt.nextGlyph(width, height, bitmask, measuredMillis)
        sw.Stop()

        If _result.Length = 0 Then
            Console.WriteLine()
        Else
            Console.WriteLine("{0} {1} {2}", _
                _result(0), _
                _result(1), _
                _result(2))
        End If
        Console.Out.Flush()

        CallNextGlyph = _result.Length
    End Function

    Function CallRender(_typesetterArt As TypesetterArt, iteration As Integer) As Integer
        Dim measuredMillis As Integer = CInt(Console.ReadLine())

        sw.Start()
        Dim _result() As Integer = _typesetterArt.render(iteration, measuredMillis)
        sw.Stop()

        Console.WriteLine(_result.Length)
        For Each _it As Integer In _result
            Console.WriteLine(_it)
        Next _it
        Console.Out.Flush()

        CallRender = 0
    End Function

End Module